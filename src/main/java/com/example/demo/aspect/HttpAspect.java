package com.example.demo.aspect;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
@Component
public class HttpAspect {
	
	private final static Logger log = org.slf4j.LoggerFactory.getLogger(HttpAspect.class);

	@Pointcut("execution(* com.example.demo.controller.*.*(..))")
	public void log(){}
	
	
	@Before("log()")
	public void before(JoinPoint joinPoint){
		
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes(); 
		HttpServletRequest request = attributes.getRequest();
		log.info("url = {}", request.getRequestURL());
		log.info("method = {}", request.getMethod());
		log.info("ip = {}",  request.getRemoteAddr());
		log.info("class_method = {}", joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName());
		log.info("args = {}", joinPoint.getArgs());
	}
	
	@After("log()")
	public void after(){
		System.out.println("after");
	}
	
	@AfterReturning(returning = "object", pointcut="log()")
	public void afterReturnint(Object object){
		//Girl g = (Girl)object;
		//System.out.println("555555555" + g.getCupSize());
		//log.info("response={}", object.toString());
	}
}
