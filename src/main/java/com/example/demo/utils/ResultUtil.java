package com.example.demo.utils;

import com.example.demo.domain.Result;

/**
 * 返回对象处理类
 * **/
public class ResultUtil {

	/**
	 * 正确返回
	 * **/
	public static Result success(Object object){
		Result result = new Result();
		result.setCode(0);
		result.setMsg("成功");
		result.setData(object);
		return result;
	}
	
	
	public static Result success(){
		return success(null);
	}
	
	/**
	 * 错误返回
	 * **/
	public static Result error(Integer code, String msg){
		Result result = new Result();
		result.setCode(code);
		result.setMsg(msg);
		result.setData(null);
		return result;
	}
}
