package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Girl;
import com.example.demo.domain.Result;
import com.example.demo.repository.GirlRepository;
import com.example.demo.service.GirlService;
import com.example.demo.utils.ResultUtil;

@RestController
public class GirlController {

//	@Value("${name}")
//	private String name;
	
//	git.oschina.net/liaoshixiong/girl
	
//	@Value("${age}")
//	private int age;
//	
//	@Value("${cupSize}")
//	private String cupSize;
	
//	@Autowired
//	private GirlProperties girl;
	
	@Autowired
	private GirlRepository girlRespository;
	
	@Autowired
	private GirlService girlService;
	
	@GetMapping(value="/girls")
	public List<Girl> getAll(){ 
		return girlRespository.findAll();
	}
	
	@GetMapping(value="/girls/{id}")
	public Girl getById(@PathVariable("id")Integer id){
		return girlRespository.findOne(id);
	}
	
	@PostMapping(value="/girls")
	//public Girl saveGirls(@RequestParam("age")Integer age,@RequestParam("name")String name,@RequestParam("cupSize")String cupSize){
	public Result<Girl> saveGirls(@Valid Girl girl, BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return ResultUtil.error(1, bindingResult.getFieldError().getDefaultMessage());
		}
		return ResultUtil.success(girl);
	}
	
	@DeleteMapping(value="girls/{id}")
	public void delById(@PathVariable("id")Integer id){
		girlRespository.delete(id);
	}
	
	@PutMapping(value="/girls/{id}")
	public Girl modifyGirls(@PathVariable("id")Integer id,
							@RequestParam(value="age")Integer age,
							@RequestParam(value="name")String name,
							@RequestParam(value="cupSize", required=true)String cupSize){
		Girl girl = new Girl();
		girl.setId(id);
		girl.setAge(age);
		girl.setCupSize(cupSize);
		girl.setName(name);
		return girlRespository.save(girl);
	}
	
	@GetMapping(value="girls/addGirls")
	public void addgirls(){
		try {
			girlService.addGirls();
		} catch (Exception e) {
			System.out.println("抛出异常" + e);
		}
	}
	
	@GetMapping(value = "girls/getAge/{id}")
	public void getAge(@PathVariable("id")Integer id) throws Exception{
		girlService.getAge(id);
	}
	
	
	
	
	
}
