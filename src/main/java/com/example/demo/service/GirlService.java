package com.example.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.demo.domain.Girl;
import com.example.demo.enums.ResultEnum;
import com.example.demo.exception.GirlException;
import com.example.demo.repository.GirlRepository;

@Service
@Transactional
public class GirlService {
	
	@Autowired
	private GirlRepository girlRepository;
	
	@Value("${resetPasswordContent}")
	String resetPasswordContent;
	
	public void addGirls() throws Exception{
//		Girl girl1 = new Girl();
//		girl1.setAge(20);
//		girl1.setCupSize("c");
//		girl1.setName("lily");
//		girlRepository.save(girl1);
//		
//		Girl girl2 = new Girl();
//		girl2.setAge(18);
//		girl2.setCupSize("sssssc");
//		girl2.setName("lily");
//		girlRepository.save(girl2);
		System.out.println(resetPasswordContent);
	}
	
	
	public Integer getAge(Integer id) throws Exception{
		Girl girl = girlRepository.findOne(id);
		if(girl.getAge() < 10){
			throw new GirlException(ResultEnum.PRIMARY_SCHOOL);
		}else if(girl.getAge() < 16){
			throw new GirlException(ResultEnum.MIDDLE_SCHOOL);
		}else{
			return girl.getAge();
		}
	}
	
}
